<?php

namespace socket\Socket;

use socket\AbstractMessageInbound as AbstractMessageInbound;

define('HOST_NAME', "0.0.0.0");
define('PORT', "8091");

class Socket
{

	private $messageInbound;

	public function __construct(AbstractMessageInbound\AbstractMessageInbound $messageInbound)
	{

		$this->messageInbound = $messageInbound;

		$this->createSocket();
	}

	private function createSocket()
	{

		$null = NULL;

		$socketResource = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
		socket_set_option($socketResource, SOL_SOCKET, SO_REUSEADDR, 1);
		socket_bind($socketResource, 0, PORT);
		socket_listen($socketResource);

		$this->messageInbound->listOfConnectedSocketClients = array($socketResource);

		while (true) {

			$newSocketArray = $this->messageInbound->listOfConnectedSocketClients;
			socket_select($newSocketArray, $null, $null, 0, 10);

			$client_ip_address = 0;

			if (in_array($socketResource, $newSocketArray)) {

				$newSocket = socket_accept($socketResource);

				$this->messageInbound->listOfConnectedSocketClients[] = $newSocket;

				$this->messageInbound->doHandshake(socket_read($newSocket, 2048), $newSocket, HOST_NAME, PORT);

				socket_getpeername($newSocket, $client_ip_address);

				$this->messageInbound->onOpen($newSocket);

				$newSocketIndex = array_search($socketResource, $newSocketArray);
				unset($newSocketArray[$newSocketIndex]);
			}

			foreach ($newSocketArray as $newSocketArrayResource) {

				$socketData = null;

				//A $socketData se le setea el valor por referencia al interior de socket_recv
				while (socket_recv($newSocketArrayResource, $socketData, 2048, 0) >= 1) {

                    $socketData = preg_replace('/[\x00-\x1F\x7F-\xFF-W]/', '', $this->unseal($socketData));
                    $this->messageInbound->onTextMessageOfClient($socketData, $newSocketArrayResource);

					break 2;
				}

				$socketData = socket_read($newSocketArrayResource, 2048, PHP_NORMAL_READ);

				//Se valida "client disconnect"
				if ($socketData === false) {

					socket_getpeername($newSocketArrayResource, $client_ip_address);
                    /** @var string $client_ip_address */
                    $this->messageInbound->onClose($client_ip_address, $newSocketArrayResource);

				}
			}
		}
		socket_close($socketResource);
	}

    private function unseal($socketData)
    {
        $length = ord($socketData[1]) & 127;
        if ($length == 126) {
            $masks = substr($socketData, 4, 4);
            $data = substr($socketData, 8);
        } elseif ($length == 127) {
            $masks = substr($socketData, 10, 4);
            $data = substr($socketData, 14);
        } else {
            $masks = substr($socketData, 2, 4);
            $data = substr($socketData, 6);
        }
        $socketData = "";
        $strlen = strlen($data);
        for ($i = 0; $i < $strlen; ++$i) {
            $socketData .= $data[$i] ^ $masks[$i % 4];
        }
        return $socketData;
    }

}
