<?php

use jerseyPhp\Http\ResponseData\ResponseData;
use jerseyPhp\Request\Request\Request;
use socket\Socket\Socket;
use socket\ChatHandler\ChatHandler;

class Loader
{

    public function __construct()
    {

        if (isset($_SERVER['argv'])) {
            if (!defined('conf')){
                define('conf', str_replace("Loader.php", "", $_SERVER['PHP_SELF']) . "app/WEB-APP/web.xml");
            }
        } else {
            if (!defined('conf')) {
                define('conf', "app/WEB-APP/web.xml");
            }
        }
    }

    /**
     * @throws Exception
     */
    static private function autoLoad(string $nameClass): void
    {

        $nameFile = str_replace("\\", "/", $nameClass) . ".php";

        $split = explode('/', $nameFile);

        $discount = count($split) >= 2 ? 2 : 1;

        $nameFile  = str_replace($split[count($split) - $discount] . "/", "", $nameFile);

        if (isset($_SERVER['argv'])) {
            //socket
            $php_self = str_replace("Loader.php", "", $_SERVER['PHP_SELF']);
            $nameFile = str_replace("\\", "/", $php_self).$nameFile;
        }

        if (file_exists($nameFile)){
            require_once $nameFile;
        }else {
            throw new Exception("Imposible importar la clase: $nameClass");
        }
    }

    public function load(): void{

        date_default_timezone_set("America/Bogota");

        spl_autoload_register(
            /**
            * @throws Exception
            */
            callback: function ($nameClass) {
            try {
                self::autoLoad($nameClass);
            } catch (Exception $e) {
                throw new Exception($e->getMessage());
            }
        });

        if (isset($_SERVER['argv'])) {
            new Socket(new ChatHandler());
        } else {
            try {
                Request::process($_GET['path'], $_GET['endpoint']);
            } catch (Exception $e) {
                print json_encode(new ResponseData(true, $e->getMessage(), null));
            }
        }
    }
}

$loader = new Loader();
try {
    $loader->load();
} catch (Exception $e) {
    print 'Imposible cargar el proyecto';
}
