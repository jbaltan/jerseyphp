<?php

namespace jerseyPhp\utils\Json;

use Exception;
use ReflectionClass;
use ReflectionException;
use stdClass;

class Json
{

    /**
     * @throws Exception
     */
    public static function convertObjectJsonToObjectPhp($json_decode, $class): ?object
    {
        return Json::convertObjectJsonToObjectPhpRecursive(json_decode($json_decode), $class);
    }

    /**
     * @throws ReflectionException
     * @throws Exception
     */
    private static function convertObjectJsonToObjectPhpRecursive($data, $class): ?object
    {

        $reflectedClass = null;

        try {
            $reflectedClass = new ReflectionClass($class);
        } catch (ReflectionException) {}

        if ($reflectedClass != null && !empty($data)) {

            $instance = $reflectedClass->newInstance();
            foreach ($data as $key => $value) {

                $nameMethod = "set" . strtoupper($key[0]) . substr($key, 1);

                try {

                    $setMethod = $reflectedClass->getMethod($nameMethod);

                    if ($value instanceof stdClass) {
                        $newValue = Json::convertObjectJsonToObjectPhpRecursive($value, $setMethod->getParameters()[0]->getType()->getName());
                        $setMethod->invoke($instance, $newValue);
                    } else {
                        $setMethod->invoke($instance, $value);
                    }
                } catch (Exception $error) {
                    throw new Exception('Imposible convert Object JSON to ' . get_class($instance) . ", conflict width: $nameMethod, details: " . $error->getMessage());
                }
            }
            return $instance;
        }
        return null;
    }
}
