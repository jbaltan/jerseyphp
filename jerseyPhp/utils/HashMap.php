<?php

class HashMap implements JsonSerializable
{

	private $arrayValue = array();
    private $arrayKey = array();

    public function getValue(string $key){
        $hash = self::getHash($key);
        $hashValue = $this->arrayValue[$hash];
        return $hashValue;
    }

    public function put(string $key, $value){
        $hash = self::getHash($key);
        $this->arrayKey[$hash] = $key;
        $this->arrayValue[$hash] = $value;
    }

    public function getAllKeys(){
        return array_values($this->arrayKey);
    }

    public function getAllValues(){
        return array_values($this->arrayValue);
    }

    public function size(){
        return count($this->arrayKey);
    }

    private static function getHash($key){
        
        if (is_object($key)){
            return spl_object_hash($key);
        }
        return $key;
    }
    
    public function jsonSerialize(){
        
        $arreglo = array();
        $keysTmp = $this->getAllKeys();
        for ($i=0; $i <$this->size(); $i++) {
            $arreglo[$keysTmp[$i]] = $this->getValue($keysTmp[$i]);
        }
        return $arreglo;
        
    }

}

