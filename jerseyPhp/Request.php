<?php

namespace jerseyPhp\Request\Request;

use Exception;
use jerseyPhp\Config\Routes as Routes;
use jerseyPhp\Http\Path\Path;
use ReflectionClass;
use ReflectionException;
use ReflectionMethod;

require_once('Config/Routes.php');
require_once('Http/Response.php');
require_once('utils/ArrayList.php');
require_once('utils/HashMap.php');

class Request
{

    const extension = ".php";

    /**
     * @throws Exception
     */
    public static function process(string $pathClassRequest, string $endpoint)
    {

        $routes = Routes\Routes::getRoutes();
        $status = false;

        foreach ($routes as $route) {

            $split = explode('\\', $route['class']);
            unset($split[count($split)-1]);

            require_once(implode("\\", $split).Request::extension);

            $reflectedClass = self::getReflectionClassByNameStringClass($route['class']);

            if (null != $reflectedClass){

                $attributesClass = $reflectedClass->getAttributes();
                if (count($attributesClass) == 0) {
                    self::launchException("Error Processing Request, problems in class: ".$route['class'].", annotations not found");
                }else{

                    $attributeAnnotation = $attributesClass[0];

                    if (null != $attributeAnnotation){

                        $newInstanceAnnotation = $attributeAnnotation->newInstance();

                        if ($newInstanceAnnotation instanceof Path){

                            $pathClass = $newInstanceAnnotation->getPath();

                            if (!empty($pathClass)) {

                                $methods = $reflectedClass->getMethods();

                                foreach ($methods as $method) {

                                    $reflectionMethod = self::getReflectionMethodByNameStringClassAndNameStringMethod($route['class'], $method->name);
                                    if (null != $reflectionMethod){

                                        $attributesMethod = $reflectionMethod->getAttributes();

                                        if (count($attributesMethod) == 0){
                                            self::launchException("Error Processing Request, problems in method: ".$method->name.", annotations not found");
                                        }else{

                                            $newInstanceAnnotationMethod = $attributesMethod[0]->newInstance();

                                            if (null != $newInstanceAnnotationMethod){

                                                $parameters = $reflectionMethod->getParameters();

                                                if ($_SERVER['REQUEST_METHOD'] == $newInstanceAnnotationMethod->getMethod()) {

                                                    if ($pathClass == $pathClassRequest && $endpoint == strtolower($newInstanceAnnotationMethod->getPath())) {

                                                        try {

                                                            $params = [];
                                                            $params[0] = null;
                                                            $i = 0;
                                                            foreach ($parameters as $parameter2) {
                                                                //if ($i > 0) {
                                                                    if (isset($_REQUEST[$parameter2->name])){
                                                                        $params[$i] = $_REQUEST[$parameter2->name];
                                                                    }else{
                                                                        throw new Exception("Error Processing Request, param: '".$parameter2->name."' no encontrado", 1);
                                                                    }
                                                                //}
                                                                $i++;
                                                            }

                                                            $response = $reflectionMethod->invokeArgs(new $route['class'](), $params);
                                                            print $response->getContent();

                                                            $status = true;
                                                            break;
                                                        }catch (ReflectionException){}
                                                    }
                                                }
                                            }else{
                                                self::launchException("Method: ".$method->name." annotation is not a PATH instance");
                                            }
                                        }

                                    }

                                    if ($status) {
                                        break;
                                    }
                                }
                            }else{
                                self::launchException("the PATH of class: ".$route['class']." is empty");
                            }
                        }else{
                            self::launchException("Class: ".$route['class']." annotation is not a PATH instance");
                        }
                    }else{
                        self::launchException("Class: ".$route['class']." annotation is null");
                    }


                }

                if ($status) break;
            }else self::launchException("Error Processing Request");

        }

        if (!$status) {
            self::launchException("Error Processing Request");
        }
        unset($_GET);
        unset($_POST);
    }

    /**
     * @param string|null $nameClass
     * @return ReflectionClass|null
     */
    private static function getReflectionClassByNameStringClass(?string $nameClass):?ReflectionClass
    {
        try {
            return new ReflectionClass($nameClass);
        } catch (ReflectionException) {}
        return null;
    }

    /**
     * @param string|null $nameClass
     * @param string|null $nameMethod
     * @return ReflectionMethod|null
     */
    private static function getReflectionMethodByNameStringClassAndNameStringMethod(?string $nameClass, ?string $nameMethod):?ReflectionMethod
    {
        try {
            return new ReflectionMethod($nameClass, $nameMethod);
        } catch (ReflectionException) {}
        return null;
    }

    /**
     * @param string|null $message
     * @return void
     * @throws Exception
     */
    private static function launchException(?string $message): void{
        throw new Exception($message, 1);
    }

}

