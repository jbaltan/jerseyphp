<?php

namespace jerseyPhp\Http\Response;

/**
 *
 */
class Response{

	private bool $status;
	private mixed $content;

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     *
     * @return self
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getContent(): mixed{
        return $this->content;
    }

    /**
     * @param mixed $content
     * @return void
     */
    public function setContent(mixed $content): void{
        $this->content = $content;
    }
}

