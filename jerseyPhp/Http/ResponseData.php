<?php

namespace jerseyPhp\Http\ResponseData;

use JetBrains\PhpStorm\Pure;
use JsonSerializable;

class ResponseData implements JsonSerializable{

    private bool $error;
    private ?string $message;
    private mixed $info;
    private ?int $typeError;
    
    public function __construct(bool $error, string $message, $info){
        $this->error = $error;
        $this->message = $message;
        $this->info = $info;
    }

    /**
     * @return bool|null
     */
    public function isError():?bool{
        return $this->error;
    }

    /**
     * @return string|null
     */
    public function getMessage():?string{
        return $this->message;
    }

    /**
     * @return mixed
     */
    public function getInfo(): mixed{
        return $this->info;
    }

    /**
     * @param bool $error
     */
    public function setError(bool $error){
        $this->error = $error;
    }

    /**
     * @param string|null $message
     */
    public function setMessage(?string $message){
        $this->message = $message;
    }

    /**
     * @param mixed $info
     */
    public function setInfo(mixed $info)
    {
        $this->info = $info;
    }

    public function getTypeError(): ?int
    {
        return $this->typeError;
    }

    /**
     * @param mixed $typeError
     * si su valor es 1 es personalizado
     */
    public function setTypeError(int $typeError): void
    {
        $this->typeError = $typeError;
    }

    #[Pure] public function jsonSerialize(){
        return get_object_vars($this);
    }

    /**
     * @return string
     */
    public function __toString():string
    {
        $json_encode = json_encode($this->jsonSerialize());
        if (gettype($json_encode) == 'string'){
            return $json_encode;
        }
        return "{}";
    }

}
