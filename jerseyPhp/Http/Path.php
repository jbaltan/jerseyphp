<?php


namespace jerseyPhp\Http\Path;


use Attribute;

#[Attribute] class Path
{

    private ?string $path;
    private ?string $method;

    public function __construct(?string $path, ?string $method)
    {
        $this->path = $path;
        $this->method = $method;
    }

    /**
     * @return string|null
     */
    public function getPath(): ?string
    {
        return $this->path;
    }

    /**
     * @param string|null $path
     * @return Path
     */
    public function setPath(?string $path): Path
    {
        $this->path = $path;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMethod(): ?string
    {
        return $this->method;
    }

    /**
     * @param string|null $method
     * @return Path
     */
    public function setMethod(?string $method): Path
    {
        $this->method = $method;
        return $this;
    }
}