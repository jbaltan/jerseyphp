<?php


namespace app\php\Controls\DemoController;

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
header('Content-Type: application/json');

use jerseyPhp\Http\Path\Path;
use jerseyPhp\Http\Response\Response;

#[Path("publicaciones", null)]
class DemoController
{

    #[Path("publicacionesByUser", "GET")]
    public function demo(int $id): Response
    {
        $response = new Response();
        $response->setContent($id);
        return $response;
    }


}
